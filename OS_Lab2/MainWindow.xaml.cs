﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace OS_Lab2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer Timer { get; set; }
        private ProcessManager Manager { get; set; }
        private bool DoAutoTicks { get => checkBoxDoAutoTicks.IsChecked ?? false; }
        public MainWindow()
        {
            InitializeComponent();

            var method = this.currentMethod;
            this.Manager = new ProcessManager(method);

            this.processGrid.ItemsSource = this.Manager.Processes;
            this.RefreshItems();

            Timer = new DispatcherTimer();
            Timer.Tick += new EventHandler(Timer_Tick);
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Start();
        }

        private ProcessManagingMethod currentMethod
        {
            get
            {
                if (checkBoxPriorityMethod.IsChecked ?? false) { return ProcessManagingMethod.PriorityDisplacing; }
                else if (checkBoxRRMethod.IsChecked ?? false) { return ProcessManagingMethod.RoundRobin; }
                else return ProcessManagingMethod.RoundRobin;
            }
        }

        

        private void RefreshItems()
        {
            labelTotalTicks.Content = this.Manager.TotalTicks.ToString();
            CollectionViewSource.GetDefaultView(this.processGrid.ItemsSource).Refresh();
        }

        private void ButtonAddProcess_Click(object sender, RoutedEventArgs e)
        {
            string name = this.textBoxNewProcessName.Text;
            int time = (int)this.upDownNewProcessTime.Value;
            int priority = (int)this.upDownNewProcessPriority.Value;
            this.Manager.Add(new Process(name, time, priority));
            this.RefreshItems();
        }

        private void CheckBoxRRMethod_Checked(object sender, RoutedEventArgs e)
        {
            this.Manager.Method = this.currentMethod;
        }

        private void ButtonManualTick_Click(object sender, RoutedEventArgs e)
        {
            this.Manager.Tick();
            this.RefreshItems();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.DoAutoTicks)
            {
                this.Manager.Tick();
                this.RefreshItems();
            }
        }

        private void CheckBoxDoAutoTicks_Checked(object sender, RoutedEventArgs e)
        {
            buttonManualTick.IsEnabled = !this.DoAutoTicks;
        }

        private void UpDownTickTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (this.Manager != null)
            {
                this.Manager.TickQuant = (int)this.upDownTickTime.Value;
            }
        }
    }
}
