﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS_Lab2
{
    enum ProcessManagingMethod { PriorityDisplacing, RoundRobin };
    class ProcessManager
    {
        public List<Process> Processes { get; set; }
        private ProcessManagingMethod _method;
        public ProcessManagingMethod Method
        {
            get => _method;
            set { ResetMethodData(); _method = value; }
        }

        public int TotalTicks { get; private set; }

        private int _tickQuant = 1;
        public int TickQuant
        {
            get => _tickQuant;
            set { _tickQuant = value > 0 ? value : 1; }
        }

        protected Queue<Process> RoundRobinQueue { get; set; }

        private Process _priorityDisplacingCurrentProcess;
        protected Process PriorityDisplacingCurrentProcess {
            get => (_priorityDisplacingCurrentProcess == null || _priorityDisplacingCurrentProcess.IsCompleted) ? null : _priorityDisplacingCurrentProcess;
            set => _priorityDisplacingCurrentProcess = value;
        }

        public ProcessManager(ProcessManagingMethod method = ProcessManagingMethod.PriorityDisplacing)
        {
            this.Processes = new List<Process>();
            this.RoundRobinQueue = new Queue<Process>();
            this.PriorityDisplacingCurrentProcess = null;
            this.Method = method;
            this.TotalTicks = 0;
        }

        /// <summary>
        /// Adds process to a managed list and performs side actions based on managing methods.
        /// </summary>
        /// <param name="target">Process to be added.</param>
        public void Add(Process target)
        {
            this.Processes.Add(target);
            switch (this.Method)
            {
                case ProcessManagingMethod.RoundRobin:
                    this.RoundRobinQueue.Enqueue(target);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Performs a "tick", picking an active process and emulating its activity.
        /// </summary>
        public void Tick()
        {
            TotalTicks += TickQuant;
            if (this.Processes.Count > 0)
            {
                switch (this.Method)
                {
                    case ProcessManagingMethod.PriorityDisplacing:
                        this.PriorityDisplacingTick();
                        break;
                    case ProcessManagingMethod.RoundRobin:
                        this.RoundRobinTick();
                        break;
                    default:
                        throw new InvalidOperationException();
                }

                this.Processes.RemoveAll(p => p.IsCompleted);
            }
        }

        /// <summary>
        /// Clears all methods' data. Useful when switching methods.
        /// </summary>
        protected void ResetMethodData()
        {
            this.PriorityDisplacingCurrentProcess = null;
            this.RoundRobinQueue.Clear();
        }

        /// <summary>
        /// Ticks all processes with the given one as an active process and the rest as passive (waiting).
        /// </summary>
        /// <param name="activeProcess">Process to be treated as active.</param>
        protected void TickProcesses(Process activeProcess)
        {
            foreach (var process in this.Processes)
            {
                process.Tick(active: process.ID == activeProcess.ID, ticks: this.TickQuant);
            }
        }

        protected void PriorityDisplacingTick()
        {
            Process currentProcess;

            // Find process with largest priority and lowest ID
            int largestPriority = this.Processes.Max(p => p.Priority);
            Process mostPriorityProcess = this.Processes.Where(p => p.Priority == largestPriority).Aggregate((cur, next) => (cur.ID < next.ID) ? cur : next);

            Process lastProcess = this.PriorityDisplacingCurrentProcess;

            // If priorities are equal, continue serving last served process
            if (lastProcess != null && lastProcess.Priority == mostPriorityProcess.Priority)
            {
                currentProcess = lastProcess;
            } else
            {
                this.PriorityDisplacingCurrentProcess = (currentProcess = mostPriorityProcess);
            }
            this.TickProcesses(currentProcess);
        }

        protected void RoundRobinTick()
        {
            if (this.Processes.Count != this.RoundRobinQueue.Count)
            {
                this.RoundRobinQueue.Clear();
                foreach (var process in this.Processes) { this.RoundRobinQueue.Enqueue(process); }
            }

            var currentProcess = this.RoundRobinQueue.Dequeue();

            this.TickProcesses(currentProcess);

            if (!currentProcess.IsCompleted)
            {
                this.RoundRobinQueue.Enqueue(currentProcess);
            }
        }
    }
}
