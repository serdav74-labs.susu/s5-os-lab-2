﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS_Lab2
{
    class Process
    {
        public Int64 ID { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
        public int WaitingTime { get; set; }
        public int RequiredTime { get; set; }

        public static Int64 lastID = 0;

        public Process(string name, int time_to_finish, int priority)
        {
            this.Name = name;
            this.RequiredTime = time_to_finish;
            this.Priority = priority;
            this.WaitingTime = 0;
            this.ID = ++Process.lastID;
        }

        public void Tick(bool active = false, int ticks = 1)
        {
            if (active) RequiredTime -= ticks; else WaitingTime += ticks;
        }

        public bool IsCompleted => RequiredTime <= 0;
    }
}
